from django.contrib import admin
from django.urls import path, include
from django.contrib.auth.views import auth_login,auth_logout

admin.site.site_header = "Blyott Admin"
admin.site.site_title = "Blyott Admin Portal"
admin.site.index_title = "Welcome to Blyott Admin Portal"

urlpatterns = [
    path('admin/', admin.site.urls),
    path('accounts/', include('django.contrib.auth.urls')),
    path('', include('asset.urls')),
]



