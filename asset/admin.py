from django.contrib import admin
from asset.models import BlyottUser, PredefinedSearch, FieldName, UserConfig, AllowedIPs, RecentSearch, SeasonName
from django.contrib.auth.models import Group

admin.site.unregister(Group)
@admin.register(BlyottUser)
class UserAdmin(admin.ModelAdmin):
    list_display = ('username', 'password')

    def has_add_permission(self, request, obj=None):
        # check if generally has add permission
        retVal = super().has_add_permission(request)
        # set add permission to False, if object already exists
        if retVal and BlyottUser.objects.exists():
            retVal = False
        return retVal

@admin.register(FieldName)
class FieldNameAdmin(admin.ModelAdmin):
    list_display = ('language',)

    def has_delete_permission(self, request, obj=None):
        if request.POST and request.POST.get('action') == 'delete_selected':
            if '1' in request.POST.getlist('_selected_action'):
                return False
            return True
        return obj is None or obj.pk != 1


admin.site.register(AllowedIPs)
admin.site.register(PredefinedSearch)
@admin.register(UserConfig)
class ConfigAdmin(admin.ModelAdmin):
    list_display = ('title', 'credentials', 'lang')

    def has_delete_permission(self, request, obj=None):
        if request.POST and request.POST.get('action') == 'delete_selected':
            if '1' in request.POST.getlist('_selected_action'):
                return False
            return True
        return obj is None or obj.pk != 1


@admin.register(RecentSearch)
class SearchAdmin(admin.ModelAdmin):
    list_display = ('query', 'searched_on', 'user', 'ip')
    list_filter = ('query', 'searched_on')



@admin.register(SeasonName)
class SeasonAdmin(admin.ModelAdmin):
    list_display = ('current_session','season_name')
    list_editable = ['season_name']

    def current_session(self, requets):
        return 'Current season'

    def has_add_permission(self, request):
        # check if generally has add permission
        retVal = super().has_add_permission(request)
        # set add permission to False, if object already exists
        if retVal and SeasonName.objects.exists():
            retVal = False
        return retVal

    def has_delete_permission(self, request, obj=None):
        return False

