from django.apps import AppConfig


class AssetConfig(AppConfig):
    name = 'asset'
    verbose_name ='PORTAL'


class CustomConfig(AppConfig):
    name = 'django.contrib.auth'
    verbose_name = ' '

