from django.db import models
from django.contrib.auth.models import User


class BlyottUser(models.Model):
    username = models.CharField(max_length=255)
    password = models.CharField(max_length=255)

    def __str__(self):
        return self.username

    class Meta:
        verbose_name = 'API'


class AllowedIPs(models.Model):
    IP = models.CharField(max_length=20)

    def __str__(self):
        return self.IP

    class Meta:
        verbose_name = 'Allowed IP address'
        verbose_name_plural = 'Allowed IP addresses'


class PredefinedSearch(models.Model):
    button_text = models.CharField(max_length=120)
    query = models.CharField(max_length=120)

    def __str__(self):
        return self.button_text

    class Meta:
        verbose_name = 'Predefined search button'


class FieldName(models.Model):
    language = models.CharField(max_length=120, unique=True)
    SearchHeader1 = models.CharField(max_length=255, help_text='I am looking for ....')
    SearchHeader2 = models.CharField(max_length=255, help_text='OR')
    SearchHeader3 = models.CharField(max_length=255, help_text='Enter here your search request')
    SearchHeader4 = models.CharField(max_length=255, help_text='Name')
    SearchHeader5 = models.CharField(max_length=255, help_text='Code')
    SearchHeader6 = models.CharField(max_length=255, help_text='Delete Everything')
    BodyHeader1 = models.CharField(max_length=255, help_text='There were')
    BodyHeader2 = models.CharField(max_length=255, help_text='results found for ')
    BodyHeader3 = models.CharField(max_length=255, help_text='Close all')
    BodyHeader4 = models.CharField(max_length=255, help_text='Show all')
    BodyHeader5 = models.CharField(max_length=255, help_text='Number of assets')
    BodyHeader6 = models.CharField(max_length=255, help_text='Floor')
    BodyHeader7 = models.CharField(max_length=255, help_text='Wing')
    BodyHeader8 = models.CharField(max_length=255, help_text='Area')
    BodyHeader9 = models.CharField(max_length=255, help_text='Location code')
    BodyHeader10 = models.CharField(max_length=255, help_text='Location name')
    BodyHeader11 = models.CharField(max_length=255, help_text='Asset name')
    BodyHeader12 = models.CharField(max_length=255, help_text='Asset code')

    def __str__(self):
        return self.language


class UserConfig(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, blank=True, null=True)
    ip = models.OneToOneField(AllowedIPs, on_delete=models.CASCADE, blank=True, null=True)
    credentials = models.ForeignKey(BlyottUser, on_delete=models.CASCADE, blank=True, null=True)
    lang = models.ForeignKey(FieldName, on_delete=models.SET_DEFAULT, default=1)

    def title(self):
        if self.user:
            return self.user.username
        elif self.ip:
            return self.ip
        else:
            return 'Default admin config'


class RecentSearch(models.Model):
    user = models.ForeignKey(User, on_delete=models.SET_NULL, blank=True, null=True)
    ip = models.ForeignKey(AllowedIPs, on_delete=models.SET_NULL, blank=True, null=True)
    query = models.CharField(max_length=255)
    searched_on = models.DateTimeField(auto_now_add=True)

    def __str__(self):
        return self.query

Seasons = (
    ('Summer','Summer'),
    ('Winter', 'Winter')
)

class SeasonName(models.Model):
    season_name = models.CharField(max_length=6, choices=Seasons, default='Summer')
    def __str__(self):
        return self.season_name

    class Meta:
        verbose_name = 'Season Setting'


