
from django.urls import path
from . import views

urlpatterns = [
    path('', views.asset_home, name='asset_home'),
    path('search/<str:q>', views.asset_search, name='asset_search'),
    path('restricted/', views.restricted, name='restricted'),
    path('recent-search/', views.recent_searches, name='recent-searches'),
]

