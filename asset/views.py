from django.shortcuts import render, redirect
from .models import BlyottUser, PredefinedSearch, UserConfig, AllowedIPs, RecentSearch, SeasonName
import requests
from itertools import groupby
import json
from datetime import date, timedelta
from django.db.models import Count
from calendar import monthrange
from django.contrib.admin.views.decorators import staff_member_required
from datetime import datetime, timedelta

asset_data = []
last_page = False
target = None


def asset_home(request):
    user = False
    IP = False
    if not request.user.is_authenticated:
        ip = get_client_ip(request)
        allowed = list(AllowedIPs.objects.filter(IP=ip))
        if len(allowed) < 1:
            return redirect('accounts/login?next=/')
        else:
            IP = True
    else:
        user = True

    if user:
        config = UserConfig.objects.filter(user=request.user.id)
    if IP:
        config = UserConfig.objects.filter(ip__IP=ip)
    if len(config) < 1:
        if request.user.is_staff:
            config = UserConfig.objects.filter(id=1)
        else:
            return redirect('restricted')
    searches = PredefinedSearch.objects.all().order_by('button_text')
    context = {
        'assets': None,
        'message': None,
        'searches': searches,
        'query': None,
        'total_count': 0,
        'config': config[0],
    }
    return render(request, 'asset/asset_search.html', context)


def asset_search(request, q):
    global asset_data, target
    total_count = 0
    user = False
    IP = False
    if not request.user.is_authenticated:
        ip = get_client_ip(request)
        allowed = list(AllowedIPs.objects.filter(IP=ip))
        if len(allowed) < 1:
            return redirect('login')
        else:
            IP = True
    else:
        user = True

    if user:
        config = UserConfig.objects.filter(user=request.user.id)
    if IP:
        config = UserConfig.objects.filter(ip__IP=ip)
    if len(config) < 1:
        if request.user.is_staff:
            config = UserConfig.objects.filter(id=1)
        else:
            return redirect('restricted')
    asset_data.clear()
    assets = []
    error_message = None
    if q != 'global':
        query = q
    else:
        query = request.GET.get('query')
        target = request.GET.get('target')

    creds = config[0].credentials
    if creds:
        headers, message = login(creds)
        if headers:
            if IP:
                RecentSearch(query=query, ip=allowed[0]).save()
            else:
                RecentSearch(query=query, user=request.user).save()
            current_season = SeasonName.objects.all().first()
            hour_offset = 3 if current_season.season_name == 'Winter' else 2
            download_assets(headers, query,hour_offset)

            asset_data = sorted(asset_data, key=lambda k: k['group'])
            total_count = len(asset_data)
            groups = groupby(asset_data, key=lambda x: x['group'])
            for item in groups:
                assets.append({'items': list(item[1])})
        if message:
            error_message = message
            assets = None
    else:
        return redirect('restricted')
    searches = PredefinedSearch.objects.all().order_by('button_text')
    config = UserConfig.objects.filter(user=request.user.id)
    if len(config) < 1:
        return redirect('restricted')
    context = {
        query: query,
        'assets': assets,
        'message': error_message,
        'searches': searches,
        'query': query,
        'total_count': total_count,
        'config': config[0]
    }
    return render(request, 'asset/asset_search.html', context)


def login(user):
    global last_page
    last_page = False
    creds = {'username': user.username, 'password': user.password}
    headers = {
        'User-Agent': 'Mozilla/5.0 (Windows NT 6.1; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36',
        'content-type': 'application/json',
        'accept': 'application/json, text/plain, */*'
    }
    login_url = 'https://api.blyott.com/login'
    try:
        resp = requests.post(login_url, data=json.dumps(creds), headers=headers)
        if resp.status_code != 200:
            return None, resp.json()['message']
        resp = resp.json()
        token = resp['Token']
        headers['token'] = token
    except Exception as e:
        return None, str(e)
    return headers, None


def download_assets(headers, query,hour_offset):
    global asset_data, last_page, target
    if target:
        if target == 'naam':
            payload = {"Page": 1, "PageSize": 1000, "GlobalSearch": "", "SortBy": "Activity", "SortOrder": "Asc", "Filters": [{"FilterBy": "AssetName", "FilterName": "Asset", "FilterContent": [query]}]}
        else:
            payload = {"Page": 1, "PageSize": 1000, "GlobalSearch": "", "SortBy": "Activity", "SortOrder": "Asc", "Filters": [{"FilterBy": "AssetCode", "Code": "Asset", "FilterContent": [query]}]}
        target = None
    else:
        payload = {"Page": 1, "PageSize": 1000, "GlobalSearch": "", "SortBy": "Activity", "SortOrder": "Asc", "Filters": [{"FilterBy": "AssetName", "FilterName": "Asset", "FilterContent": [query]}]}
    endpoint = 'https://api.blyott.com/assets'
    response = requests.post(endpoint, data=json.dumps(payload), headers=headers).json()
    total_assets = response['TotalItems']
    total_assets_page = int(total_assets / 1000) + 2

    parse(response,hour_offset)
    if total_assets_page > 1:
        for i in range(2, total_assets_page):
            if last_page:
                break
            payload['Page'] = i
            assets = requests.post(endpoint, data=json.dumps(payload), headers=headers).json()
            parse(assets,hour_offset)


def parse(assets_response,hour_offset):
    global asset_data, last_page
    for asset in assets_response['Items']:
        row = {}
        try:
            row['name'] = asset['Name']
        except Exception as e:
            row['name'] = ''
        try:
            row['code'] = asset['Code']
        except Exception as e:
            row['code'] = ''
        try:
            if asset['LocationName']:
                row['location_name'] = asset['LocationName']
            else:
                row['location_name'] = 'unknown'
        except Exception as e:
            row['location_name'] = 'unknown'
        try:
            if asset['TimeLastSeen']:
                last_time_seen = asset['TimeLastSeen']
                dt = datetime.strptime(last_time_seen, "%Y-%m-%dT%H:%M:%S.%f") + timedelta(hours=hour_offset)
                row['timelastseen'] = dt
            else:
                row['timelastseen'] = ''
        except:
            row['timelastseen'] = ''
        try:
            if asset['Activity'] == 1:
                row['activity'] = '5 min or less'
            if asset['Activity'] == 2:
                row['activity'] = '5-120 min'
            if asset['Activity'] == 3:
                row['activity'] = '120+ min'
            if asset['Activity'] == 4:
                row['activity'] = 'Never seen'
        except Exception as e:
            row['activity'] = 'Never seen'
        try:
            row['location_code'] = asset['LocationCode']
            try:
                p1 = asset['LocationCode'].split('/')[0].split('.')[2:]
                p2 = asset['LocationCode'].split('/')[1].split('.')[:2]
                group = ''.join(p1 + p2)
            except:
                group = 'unknown'
                row['location_code'] = 'unknown'
            row['group'] = group
        except Exception as e:
            row['location_code'] = 'unknown'
            row['group'] = 'unknown'
        if row['group'] == 'unknown' and asset['Activity'] != 3:
            last_page = True
            continue
        for item in asset['CustomFields']:
            try:
                row[item['Name']] = item['Value']
            except Exception as e:
                row[item['Name']] = ''
        if row not in asset_data:
            asset_data.append(row)


def get_client_ip(request):
    x_forwarded_for = request.META.get('HTTP_X_FORWARDED_FOR')
    if x_forwarded_for:
        ip = x_forwarded_for.split(',')[0]
    else:
        ip = request.META.get('REMOTE_ADDR')
    return ip


def restricted(request):
    return render(request, 'asset/restricted.html')


@staff_member_required()
def recent_searches(request):
    week_start = date.today()
    week_start -= timedelta(days=week_start.weekday())
    week_end = week_start + timedelta(days=7)
    today_date = date.today()
    month_start = today_date.replace(day=1)
    month_end = today_date.replace(day=monthrange(today_date.year, today_date.month)[1])
    week_result = RecentSearch.objects.values('query').filter(searched_on__gte=week_start, searched_on__lt=week_end).order_by('query').annotate(total=Count('query'))
    weekly_total = sum(d['total'] for d in week_result)
    month_result = RecentSearch.objects.values('query').filter(searched_on__gte=month_start, searched_on__lte=month_end).order_by('query').annotate(total=Count('query'))
    monthly_total = sum(d['total'] for d in month_result)

    context = {
        'week_result': week_result,
        'month_result': month_result,
        'weekly_total': weekly_total,
        'monthly_total': monthly_total
    }
    return render(request, 'asset/recent_searches.html', context)


